var server = require("server");
var URLUtils = require("dw/web/URLUtils");

server.get("Show", function (req, res, next) {
    var template = "testing";
    res.render(template);
    next();
});

server.get("Product", function (req, res, next) {
    var template = "product";
    var myproduct = { pid: "701644329402M" };
    var ProductFactory = require("*/cartridge/scripts/factories/product");
    var product = ProductFactory.get(myproduct);

    res.render(template, {
        product: product,
    });
    next();
});

server.get("Basket", function (req, res, next) {
    var template = "basket";
    var BasketMgr = require("dw/order/BasketMgr");
    var currentBasket = BasketMgr.getCurrentBasket();
    if (!currentBasket) {
        res.setStatusCode(500);
        res.json({
            error: true,
            redirectUrl: URLUtils.url("Cart-Show").toString(),
        });
    }
    var productLineItems = currentBasket.allProductLineItems;
    res.render(template, {
        productLineItems: productLineItems,
    });
    next();
});

server.get("Tags", function (req, res, next) {
    var template = "tags";
    var cond = true;
    var printMeTrue = "Its TRUE!";
    var printMeFalse = "Its FALSE!";
    res.render(template, {
        printMeTrue: printMeTrue,
        printMeFalse: printMeFalse,
        cond: cond,
    });
    next();
});

server.get("Variables", function (req, res, next) {
    var template = "variables";
    res.render(template);
    next();
});

server.get("LocalInclude", function (req, res, next) {
    var template = "localInclude";
    res.render(template);
    next();
});

server.get("RemoteInclude", function (req, res, next) {
    var template = "remoteInclude";
    res.render(template);
    next();
});

server.get("Decorator", function (req, res, next) {
    var template = "decorator";
    var myproduct = { pid: "701644329402M" };
    var ProductFactory = require("*/cartridge/scripts/factories/product");
    var product = ProductFactory.get(myproduct);

    res.render(template, {
        product: product,
    });
    next();
});

server.get("Loop", function (req, res, next) {
    var template = "loop";
    var loop = ["Havaianas", "Sandals", "Shoes"];
    res.render(template, {
        loop: loop,
    });
    next();
});

server.get("Category", function (req, res, next) {
	var template = "category";
	var CatalogMgr = require("dw/catalog/CatalogMgr");
	var ProductFactory = require("*/cartridge/scripts/factories/product");
	var ProductSearchModel = require("dw/catalog/ProductSearchModel");
	var apiProductSearch = new ProductSearchModel();
	var PagingModel = require("dw/web/PagingModel");
	var category = CatalogMgr.getCategory("womens");
	var pagingModel;
	var products = [];
	apiProductSearch.setCategoryID(category.ID);
	apiProductSearch.search();
	pagingModel = new PagingModel(
			apiProductSearch.getProductSearchHits(),
			apiProductSearch.count
	);
	pagingModel.setStart(1);
	pagingModel.setPageSize(100);

	var iter = pagingModel.pageElements;
	while (iter !== null && iter.hasNext()) {
			productSearchHit = iter.next();
			product = ProductFactory.get({
					pid: productSearchHit.getProduct().ID,
			});
			products.push(product);
	}

	res.render(template, { products: products, category: category });

	next();
});

server.get("Styled", function (req, res, next) {
	var template = "styled";
	res.render(template);
	next();
});
	
server.get("Cache", function (req, res, next) {
	var template = "cache";
	res.render(template);
	next();
});

module.exports = server.exports();
