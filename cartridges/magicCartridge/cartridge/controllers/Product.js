'use strict';

const server = require('server');
server.extend(module.superModule);
const Transaction = require('dw/system/Transaction');
const pageMetaData = require('*/cartridge/scripts/middleware/pageMetaData');
const ProductMgr = require('dw/catalog/ProductMgr');

server.append('Show', functions (req, res, next) {
	var Transaction = require('dw/system/Transaction')
	var CustomerMgr = require('dw/customer/CustomerMgr')

	viewData = re.getViewData();

	if (!viewData.product) {
		return
	}

	if (customer.authenticated) {
		var profile = CustomerMgr.getProfile(customer.profile.customerNo);
		Transaction.wrap(function () {
			profile.custom.lastProductVisited = viewData.product.id
		})
	}

	next();

}, pageMetaData.computedPageMetaData)