"use strict";
var server = require("server");
var URLUtils = require("dw/web/URLUtils");

server.get("Start", function (req, res, next) {
    var actionUrl = URLUtils.url("Newsletter-SaveNewsLetter");

    res.render("SFRAFormTemplate", {
        actionUrl: actionUrl,
    });
    next();
});

server.post("SaveNewsLetter", function (req, res, next) {
    var email = req.form.email;
    var Transaction = require("dw/system/Transaction");
    var CustomObjectMgr = require("dw/object/CustomObjectMgr");
    var template = "success";
    var data = {};
    try {
        Transaction.wrap(function () {
            var notificationObject = CustomObjectMgr.createCustomObject(
                "newsletterCustom",
                email
            );
            notificationObject.custom.email = email;
        });
        data = {
            email: email,
        };
    } catch (e) {
        data = {
            error: "E-mail already exist",
        };
        template = "failure";
    }

    res.render(template, data);

    next();
});

module.exports = server.exports();
